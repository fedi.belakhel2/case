import { defineStore } from "pinia"

export interface Process {

  id: string
  type: string
  order: number
  triggerType?: string
  actionType?: string
  options?: {
    url?: string
    parameters?: string
  }

}

export interface Automation {
  id: string
  name: string
  created_at?: Date
  created_by?: string
  updated_at?: Date
  updated_by?: string
  process: Process[]

}

export type Automations = Record<string, Automation>

export const automationsStore = defineStore("automations", () => {
  const automations = ref<Automations>({})

  /**
   * Load automations from the localStorage
   */
  function loadAutomations() {
    const data = localStorage.getItem("automations")
    if (!data)
      return
    automations.value = JSON.parse(data) || []
  }

  function addAutomations(item: Automation) {
    automations.value = { ...automations.value, [item.id]: { ...item, created_at: new Date(), created_by: "me" } }
    refrechLocalStorage()
  }

  function updateAutomation(item: Automation, id: string) {
    automations.value[id] = { ...item, updated_at: new Date(), updated_by: "me" }
    refrechLocalStorage()
  }

  function deleteAutomation(id: string) {
    delete automations.value[id]
    refrechLocalStorage()
  }

  function refrechLocalStorage() {
    localStorage.setItem("automations", JSON.stringify(automations.value))
  }

  loadAutomations()

  return { automations, loadAutomations, addAutomations, deleteAutomation, updateAutomation, refrechLocalStorage }
})
