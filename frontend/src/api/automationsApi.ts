import axios from "axios"

const apiBaseURI = import.meta.env.VITE_BACKEND_URL_GITPOD ?? "http://localhost:8080"

export const autoApiCall = async (url = "", parameters = "") => {
  try {
    const response = await axios.post(`${apiBaseURI}/automations/run`, {
      url,
      parameters,
    })
    return response.data
  } catch (error: any) {
    return { data: {}, msg: "", errorMsg: error.message }
  }
}
