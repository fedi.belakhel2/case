# Product to tech case / Bonx

[[_TOC_]]

## Exercise context

The goal of this exercise is double : 
- Give you insights about how we work at Bonx and, should we decide to work together, contribute to accelerate your onboarding
- Help us understand if we could mutually be a good fit together    
   
   
This document is a Product Brief - a product brief drives the implementation of a feature at Bonx, from its product specification to its tech implementation.   
   
   
A Product Briefs in Bonx is equivalent to a “Pitch”' in the ‘Shape-up’ methodology if you know it (it was invented by Basecamp and used at multiple very successful startups with very high engineering standards such as Alan). We run on a modified Shape-up methodology.   
   
          
About the exercise :
- This base repository is already functional, simply fork it !
- Exceptionally, you should directly push your commits on main (in real conditions, we work with feature branches but for the sake of simplicity no need to do this here)
- Feel free to leverage anything you’d use on the job - this means libraries, stackoverflow, etc. But you should be able to explain any bit of code, no direct copy-paste from ChatGPT or Github Copilot abuse (it's pretty obvious anyways).
- The exercice should not take more than 2h
- There is no trap, no fancy stuff, just what the job would be
- You’re building a feature for a no-code platform - keep this in mind, nothing about data or processes can be hardcoded !   
    
What we evaluate :
- Your ability to think of problems first before diving into coding
- Your ability to ask questions to lift any possible confusion
- Your ability to build a working feature that looks and feels professional (hint : about the look, leveraging basic Tailwind classes with no specifics helps a lot).
- Your ability to write clean, simple, readable code
- Your ability to write robust code, including code that handles error well   
   
   
   
## Product Brief
### Pain points we want to address in the feature
- In every factory we've signed as clients, there are a lot of cases where users are manually doing a lot of repetitive actions in Bonx. By doing so manually, they waste a lot of time which has negative impact for the factory efficiency & margins, and errors happen frequently. 
- Every time, we observed that power users know well about those painful and repetitive routines that factory managers or operators are dealing with, and power users are able to both describe well :
  1. What triggers the start of a given repetitive tasks in terms of data (example: "When any order for client A is entering production, we need to edit the invoice for them and send them right away")
  2. What are the exact series of steps that need to be performed to automate the step. (example: "You first need to create to create the invoice, export it as PDF then send it by email to the point of contact at client A")
- We've also observed there isn't any way to build a "verticalized" feature for these types of repetitives routines currently done manually, because they are completely different from one factory to the other. Also, there can be many different types of repetitive routines within a single factory. To adress this complexity, and because it fits very well with our vision, we want to give Power Users the full ability to craft those routines themselves.

### Solutions 

We want to build a feature that we'll call "Automations".

Automations, high-level, are composed of 2 concepts :
- A trigger, that describes when an automation should start (on which conditions basically)
- A sequence of actions, that describe what the automation should do once fired up.

High-level, there are 2 user stories we want to implement in the feature :
- A Power User can build a new automation **with a no-code UI in the frontend**
- Any User can run the workflow with a Run button

Because we feel this feature could be used in very surprising ways and lead to a wild variety of unexpected cases, our appetite for this feature is to build an MVP of it, very fast, in 2h max, put it in production then iterate from actual user feedback and real use cases.

To achieve this, we voluntarily cut down scope to only those two items :
- Trigger : the only trigger for now is a "manual launch" with a button that could be available on the automation page 
- Actions : we'll implement those two actions :
   - External API call : a simple action that calls an external API - it serves to replace the task of a regular user browsing a web page to fetch an information. The only parameter of this action is the URL on which to perform a GET.
   - Popup : a simple action that triggers the opening of a popup to inform the user. It has a single parameter "Message". This parameter can be configured to display data from one or more previous actions (for instance, the "External API call" action.)

### Exercise preparation

**It's best if you can prepare this part before the first call with Jean-Baptiste or Rémi to onboard you on the tech case.**


- The product brief at this point is very rough, and deals very little with how from an engineering standpoint the feature can get built. From an engineering POV :
  - What are different possible ways to build it ? Which one would you pick, and why ? 
  - What parts of the feature would deserve some clarification ?
  - Do you have questions about the brief ?
  - Do you have feedback to improve the brief ?
- Prepare a quick wireframe of how you'd like to structure the feature (We love excalidraw.com for this - but a very rough paper and pen mockup will do it as well) (_in practice, this part would be done by UX designer or product managers mostly, but tech sometimes chime in and it's important for us that you have at least some fundamentals on this_)
- What natural extensions to this feature come to your mind ? (_do not implement them here_)


### Constraints

- Ensure the code works using the following flow (BEWARE : this flow should not be hardcoded - this is a feature for a no-code platform)
  - Action #1 : “External API” action with the following parameters   
    URL : https://api.spacexdata.com/v4/payloads/5eb0e4b5b6c3bb0006eeb1e1 (SpaceX API)
  - Action #2 : “Popup action” with the following parameters:   
    Message : “Manufacturer of current payload : {{ steps[0].manufacturers }}”

The data between {{ }} in action #2 should be rendered with data from action #1.
Users should see the popup show up when they launch the automation.


## Get started

1. Fork the base repo https://gitlab.com/bbqx/case
2. To get started faster, you can use [Gitpod](https://gitpod.io) which will set up your development environment for you    
   Otherwise, follow these instructions :   
   a. We use Node 18.14.2   
   b. Install turbo globally : `npm install -g turbo`  
   b. Install pnpm   
   c. Install dependencies   
   d. Launch dev server with `pnpm run dev`
   

## Last but not least

When you're done, send us by email at jb@bonx.tech (or remi@bonx.tech if Jean-Baptiste was not your main point of contact) the link to your fork with all the other elements you think are necessary: ​​thoughts, drafts, diagrams, ...
     
Good luck and thank you for participating!
