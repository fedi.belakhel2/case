import type { FastifyInstance } from "fastify"
import axios from "axios"
import { trim } from "lodash-es"

const run = async (request: any, reply: any) => {
  const { url, parameters } = request.body
  let results = []

  const errorList: string[] = []
  try {
    if (!url || !parameters)
      throw new Error("invalid input")

    const response = await axios.get(url)

    const parametersList = trim(parameters).split(",")

    results = parametersList.map((parameter: string) => {
      if (!response.data[parameter]) {
        errorList.push(`${parameter} doesn't exist`)
        return ""
      }
      return `${parameter} : ${response.data[parameter]}`
    })
  } catch (error: any) {
    return reply.status(401).send({ errorMsg: error.message })
  }
  return reply.status(200).send({
    status: "ok",
    data: results.join(","),
    msg: "",
    errorMsg: errorList.join(","),
  })
}

export default (app: FastifyInstance, _: any, done: any) => {
  app.post("/run", run)
  done()
}
